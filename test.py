from unittest.mock import MagicMock
from price_crawler import Crawler
import unittest
import os


class TestCrawler(unittest.TestCase):
    def setUp(self):
        self.crawler_file = "test_file.txt"
        self.max_links = 3

    def tearDown(self):
        if os.path.exists(self.crawler_file):
            os.unlink(self.crawler_file)

    def test_start_crawler(self):
        crawler = Crawler('test', max_links=self.max_links)
        page_string = """  <a href="test1"></a> <a href="test2"></a> <a href="test3"></a> """
        crawler.get_page_string = MagicMock(return_value=page_string)
        crawler.start_crawler()
        self.assertEqual(len(crawler.visited_links), self.max_links)

    def test_get_page_data(self):
        crawler = Crawler('', self.max_links)
        page_string = """   <div class="box mitem">
                                <div class="mtitle"> <a title="test_title"></a> </div>
                                <div class="price">PHP 1.00<span></span></div>
                            </div> """
        page_data = crawler.get_page_data(page_string)
        self.assertEqual(page_data[0], "{} - {}".format("test_title", "PHP 1.00"))

    def test_get_page_links(self):
        crawler = Crawler('', self.max_links)
        page_string = """  <a href="test1"></a> <a href="test2"></a> <a href="test3"></a> """

        crawler_links = [link.get('href') for link in crawler.get_page_links(page_string)]
        expected_links = ["test1", "test2", "test3"]
        self.assertListEqual(expected_links, crawler_links)

    def test_write_data(self):
        crawler = Crawler('', self.max_links, file=self.crawler_file)
        test_string = "my test string"
        crawler.write_page_data([test_string])
        self.assertTrue(os.path.exists(self.crawler_file))

        with open(self.crawler_file) as f:
            content = f.readline()
            self.assertEqual(content.rstrip(), test_string)


if __name__ == '__main__':
    unittest.main()
