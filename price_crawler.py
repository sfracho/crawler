from urllib.request import urlopen
from urllib.error import URLError
from bs4 import BeautifulSoup
import os


class Crawler:
    def __init__(self, base_site, max_links, context=None, file=None):
        """
        Initializes the Crawler
        :param base_site:  the site to gather information from
        :param max_links: the maximum links to traverse before stopping
        :param context: a string keyword to use for searching prices on relevant items
        :param file: string file name for results
        :return:
        """
        self.base_site = base_site
        self.max_links = max_links
        self.context = context
        self.file = file
        self.visited_links = list()

    def start_crawler(self):
        """
        Starts the crawler
        :return:
        """
        self.__visit_link(self.base_site)

    def __visit_link(self, link):
        """
        Recursive class function that traverses html links depth-first.
        Gathers all links in the page and collects data from links with relevant keywords
        :param link: String url
        :return:
        """
        if len(self.visited_links) >= self.max_links:
            return
        if len(self.visited_links) < 1:
            self.base_site = link

        self.visited_links.append(link)
        print("New Link Found:", link)

        page_string = self.get_page_string(link)
        page_data = self.get_page_data(page_string)
        self.write_page_data(page_data)

        page_links = self.get_page_links(page_string)
        for anchor in page_links:
            new_link = anchor.get('href', '')

            unvisited = new_link not in self.visited_links
            base_site = self.base_site in new_link
            related = True if not self.context else self.context in new_link.lower()

            if new_link and unvisited and base_site and related:
                self.__visit_link(new_link)

    def get_page_string(self, link):
        """
        Opens the link and retrieves the contents of the html page in string form
        :param link: String url
        :return:
        """
        try:
            response = urlopen(link)
        except (ValueError, URLError, AttributeError):
            response = None

        htmlString = str()
        if response and 'text/html' in response.getheader('Content-Type'):
            htmlBytes = response.read()
            htmlString = htmlBytes.decode("utf-8")
        return htmlString

    def get_page_links(self, page_string):
        """
        Gathers all the links in an html page, returns a list of Beautiful Soup Tag objects that
        represent the anchor tags
        :param page_string: html string
        :return:
        """
        soup = BeautifulSoup(page_string, 'html.parser')
        links = soup.find_all('a')
        return links

    def get_page_data(self, page_string):
        """
        Customized for ebay.ph. Searches html string for specific tags that have pricing information for items.
        Returns a list of strings containing the item name and item price.
        :param page_string: html string
        :return:
        """
        page_data = list()
        soup = BeautifulSoup(page_string, 'html.parser')
        div_parents = soup.find_all('div', {'class': "box mitem"})
        for div_parent in div_parents:
            name = div_parent.find('div', {'class': 'mtitle'})
            price = div_parent.find('div', {'class': 'price'})
            text = "{} - {}".format(name.find('a').get('title'), price.contents[0])
            page_data.append(text)

        return page_data

    def write_page_data(self, page_data):
        """
        Opens a file and writes each element of the retrieved data on a file line by line
        :param page_data: list of strings to write
        :return:
        """
        if not self.file:
            for text in page_data:
                print(text)
        else:
            with open(self.file, "a+") as text_file:
                for text in page_data:
                    print(text, file=text_file)


def main():
    base_site = "http://www.ebay.ph"
    max_links = 20
    file = "prices.txt"
    if os.path.exists(file):
        os.unlink(file)
    crawler = Crawler(base_site, max_links, "ipad", file)
    crawler.start_crawler()


if __name__ == "__main__":
    main()
